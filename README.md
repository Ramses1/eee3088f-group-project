# EEE3088F Group Project

This is a repository for Group 48's project for EEE3088F offered by UCT. 
Group members include Brandon Ferreira (FRRBRA002), Ramses Tagne (TGNRAM001) and Given Kibanza (KBNGIV001)

The aim of the project is the design of a microHat for a pi-Zero. The microHat will act as a back-up power module for the pi-Zero if the mains supply
fails. The design consists of three main sub-modules namely the Power-circuitry, the op-amp based amplifier and status LED's.

# How to use repository

All files must be added to the directory they are concerned with. i.e) KiCad files must be placed in the KiCad folder. This is done to keep order 
and allow for easy information navigation.

When adding file, they will first be submitted for approval to ensure they are the lastest version and that all additions are valid.

# Bill of materials

The following is a list of components and materials used for the design of the microHat. The Bill of Materials has been divided into the seperate
sub-module sections.

## Power circuitry

1. x1 - LT317A
2. x1 - LT1074
3. x2 - BC547
4. x1 - 0.8 ohm resistor
5. x2 - 100 ohm resistor
6. x1 - 240 ohm resistor
7. x2 - 1k ohm resistor
8. x1 - 1k8 ohm resistor
9. x1 - 2k7 ohm resistor
10. x1 - 10k ohm resistor
11. x5 - RBR3MM40A (Diode)
12. x1 - MBR745 (Diode)
13. x1 - 50 uH inductor
14. x2 - 1000 uF capacitor
15. x1 - 0.01 uF capacitor

## Op-amp based amplifier circuitry

1. x2 - 10k ohm resistor
2. x1 - 20k ohm resistor
3. x1 - 8k2 ohm resistor

## Status LED circuitry

1. x3 - Green LED
2. x2 - 1k ohm resistor
3. x1 - 2k2 ohm resistor

## Additional components

1. x1 - Store bought wall plug-in AC-to-DC converter

# Additional information

Further details to be added
